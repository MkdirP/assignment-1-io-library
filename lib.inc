section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 
    syscall     
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax            
.loop:
    cmp byte [rdi + rax], 0 
    je .end                 
    inc rax                 
    jmp .loop
.end:    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length  
    mov rdx, rax        
    mov rsi, rdi        
    mov rax, 1          
    mov rdi, 1          
    syscall             
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi        
    mov rax, 1      
    mov rdi, 1      
    mov rsi, rsp    
    mov rdx, 1      
    syscall         
    pop rdi         
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA        
    jmp print_char    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rdx, rdi   
	xor rcx, rcx		
	dec rsp
	mov byte[rsp], 0
.loop:	       
    dec rsp		
    inc rcx
    mov rax, rdx	 
    xor rdx, rdx
    mov r11, 10
    div r11    
    add rdx, zero_code	
    mov [rsp], dl
    mov rdx, rax
    test rdx, 0		
    jne .loop                
    mov rdi, rsp   		
    push rcx		
    call print_string
    pop rcx	       
    add rsp, rcx   		
    inc rsp	       		
    ret	                            

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0      
    jge .positive   
    push rdi        
    mov rdi, '-'    
    call print_char 
    pop rdi         
    neg rdi         
.positive:
    jmp print_uint 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax              
.loop:				
    mov dl, byte [rsi+rax]
    cmp byte [rdi+rax], dl 
    jne .false
    cmp byte [rdi+rax], 0
    je .true
    inc rax               
    jmp .loop            
.false:
    xor rax, rax
    ret
    .true:
    mov rax, 1
    ret	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax    
    xor rdi, rdi    
    push 0         
    mov rsi, rsp   
    mov rdx, 1     
    syscall       
    pop rax        
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx            
.loop:
    push rdi                
    push rsi
    push rcx
    call read_char          
    pop rcx                 
    pop rsi
    pop rdi
    cmp rax, 0x20           
    je .space
    cmp rax, 0x9
    je .space
    cmp rax, 0xA
    je .space
    cmp rax, 0              
    je .end
    mov [rdi + rcx], rax    
    inc rcx                 
    cmp rcx, rsi            
    jge .error
    jmp .loop
.space:
    cmp rcx, 0              
    je .loop                
    jmp .end
.end:
    xor rax, rax            
    mov [rdi + rcx], rax
    mov rax, rdi           
    mov rdx, rcx            
    ret                     
.error:
    xor rax, rax           
    xor rdx, rdx           
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax		
	xor rdx, rdx		
	xor rcx, rcx		
.num:
    xor rcx, rcx
    mov cl, [rdi+rdx]	
    cmp cl, 0x30	
    jb .end 
    cmp cl, '9'     
    ja .end
    sub rcx, zero_code	
    mov r11, 10		;x10
    push rdx
    mul r11
    pop rdx
    add rax, rcx	
    inc rdx
    jmp .num
.end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
   cmp byte[rdi], '+'	
	je .parse
	cmp byte[rdi], '-'
	jne parse_uint	  	
.parse:
    push rdi
    inc rdi		  
    call parse_uint	  
    pop rdi
    inc rdx
    cmp byte[rdi], '+'	
    je .end
    neg rax		  
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length      
    inc rax                 
    cmp rax, rdx           
    jg .buff       
    xor rcx, rcx           
.loop:
    mov rdx, [rdi + rcx]    
    mov [rsi + rcx], rdx   
    inc rcx                 
    cmp byte[rdi + rcx], 0  
    jne .loop              
    ret
.buff:
    xor rax, rax          
    ret
